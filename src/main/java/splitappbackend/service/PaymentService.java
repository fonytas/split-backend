package splitappbackend.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import splitappbackend.model.*;
import splitappbackend.repository.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;


@Service("paymentService")
public class PaymentService {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    OweRepository oweRepository;

    @Autowired
    SettleUpRepository settleUpRepository;



    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public String generateBillID() {
        Random random = new Random();
        StringBuilder builder = new StringBuilder(6);
        for (int i = 0; i < 6; i++) {
            builder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }
        return builder.toString();
    }


    public ResponseEntity saveBalanceEqually(String username,
                                             String eventName,
                                             BigDecimal totalBalance,
                                             String paidBy,
                                             ArrayList<String> members,
                                             String groupName,
                                             HashMap<String, Object> summary) {

        try {

            String billID = generateBillID();

            Group group = groupRepository.findByGroupName(groupName);

            BigDecimal roundBalance = new BigDecimal(totalBalance.toBigInteger());
            MathContext mc = new MathContext(3);
            roundBalance = roundBalance.round(mc);
            Event event = new Event(eventName, new Date(), roundBalance, username, billID);
            addEventDetailInGroup(groupName, event);

            // TODO: Add members in events
            addMemberInEvents(members, event);






            User userPaid = userRepository.findByUserName(paidBy);

            for (HashMap.Entry<String, Object> entry : summary.entrySet()) {


                String name = entry.getKey();
                BigDecimal amount = new BigDecimal(entry.getValue().toString());
                Payment payment;
                if (name.equals(paidBy)) {
                    payment = new Payment(billID, name, roundBalance, amount, group.getGroupUUID());
                } else {
                    payment = new Payment(billID, name, new BigDecimal(0.00), amount, group.getGroupUUID());
                }
                paymentRepository.save(payment);

                // TODO : Save the debt summary to owe repository !
                if (!paidBy.equals(name)) {
                    User debtor = userRepository.findByUserName(name);
                    Owe owe = new Owe(debtor.getUserUUID(), userPaid.getUserUUID(), amount, group.getGroupUUID());

                    oweRepository.save(owe);

                }

            }
            return ResponseEntity.ok(null);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());

        }

    }


    private void addEventDetailInGroup(String groupName, Event event) {
        Group group = groupRepository.findByGroupName(groupName);
        System.out.println(group);
        event.setGroup(group);
        eventRepository.save(event);
        Set<Event> eventSet = group.getEvents();
        eventSet.add(event);
        group.setEvents(eventSet);
        groupRepository.save(group);

    }

    private void addMemberInEvents(ArrayList<String> members, Event event) {

        Set<User> userInEvent = new HashSet<>();
        for (String member : members) {

            User user = userRepository.findByUserName(member);
            userInEvent.add(user);

            Set<Event> userEvents = user.getEvents();
            userEvents.add(event);
            user.setEvents(userEvents);
            userRepository.save(user);
        }
        event.setMembersInEvent(userInEvent);
        eventRepository.save(event);

    }

    public ResponseEntity getYourBalanceSummary(String username) {

        User user = userRepository.findByUserName(username);
        Long userUUID = user.getUserUUID();
        HashMap<String, BigDecimal> summary = new HashMap<>();


        // find you are owed
        Set<Owe> youAreOweSet = oweRepository.findAllByPaidByUUID(userUUID);
        BigDecimal youAreOwedAmount = new BigDecimal(0.00);
        for (Owe owe : youAreOweSet) {
            BigDecimal amount = new BigDecimal(String.valueOf(owe.getAmount()));
            youAreOwedAmount = youAreOwedAmount.add(amount);
        }
        summary.put("youAreOwed", youAreOwedAmount);


        // find you owe
        Set<Owe> youOweSet = oweRepository.findAllByDebtorUUID(userUUID);
        BigDecimal youOwe = new BigDecimal(0.00);
        for (Owe owe : youOweSet) {
            BigDecimal amount = new BigDecimal(String.valueOf(owe.getAmount()));
            youOwe = youOwe.add(amount);
        }
        summary.put("youOwe", youOwe);

        return ResponseEntity.ok(summary);

    }

    public ResponseEntity getBalanceSummaryOfFriends(String username, String friendName) {


        User user = userRepository.findByUserName(username);
        User friend = userRepository.findByUserName(friendName);



        Set<Owe> allPaidBy = oweRepository.findAllByPaidByUUIDAndDebtorUUID(user.getUserUUID(), friend.getUserUUID());
        Set<Owe> allDebt = oweRepository.findAllByPaidByUUIDAndDebtorUUID(friend.getUserUUID(), user.getUserUUID());

        BigDecimal paidBy = addUpBalance(allPaidBy);
        BigDecimal debt = addUpBalance(allDebt);
        BigDecimal finalBalance = paidBy.subtract(debt);


        HashMap<String, BigDecimal> summary = new HashMap<>();
        summary.put("paidBy", paidBy);
        summary.put("debt", debt);
        summary.put("finalBalance", finalBalance);



        return ResponseEntity.ok(summary);
    }

    public BigDecimal addUpBalance(Set<Owe> oweSet) {
        BigDecimal total = new BigDecimal(0.00);

        for (Owe owe : oweSet) {
            total = total.add(owe.getAmount());
        }

        return total;

    }

    public ResponseEntity settleUp(String username, String friendName, BigDecimal totalBalance) {

//    public Owe(Long debtorUUID, Long paidByUUID, BigDecimal amount, Long groupUUID) {

        try {
            User user = userRepository.findByUserName(username);
            User friend = userRepository.findByUserName(friendName);

            Owe owe = new Owe(friend.getUserUUID(), user.getUserUUID(), totalBalance);

//                public Owe(Long debtorUUID, Long paidByUUID, BigDecimal amount) {

//                public SettleUp(BigDecimal amount, String giver, String receiver) {

            SettleUp settleUp = new SettleUp(totalBalance, username, friendName);

            settleUpRepository.save(settleUp);


            oweRepository.save(owe);


            return ResponseEntity.ok(null);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
//    private ArrayList<HashMap> addData(Set<SettleUp> settleUpSet) {
//        ArrayList<HashMap> newList = new ArrayList<>();
//
//        for (SettleUp settleUp : settleUpSet) {
//            HashMap<String, Object> map = new HashMap<>();
//
//            map.put("Date", settleUp.getDate());
//            map.put("Giver", settleUp.getGiver());
//            map.put("Receiver", settleUp.getReceiver());
//            map.put("Amount", settleUp.getAmount());
//
//            newList.add(map);
//
//        }
//
//        return newList;
//
//    }
    public static <T> Set<T> mergeSet(Set<T> a, Set<T> b)
    {
        return new HashSet<T>() {{
            addAll(a);
            addAll(b);
        } };
    }

    public ResponseEntity getSettleUpHistory(String username) {

        try {
            // You give Friend
            Set<SettleUp> settleUpSet = settleUpRepository.findAllByGiver(username);
//            Set<SettleUp> settleUpSet = settleUpRepository.findAllByGiverAndReceiver(username, friendName);


            // Friend give to You
            Set<SettleUp> settleUpSet2 = settleUpRepository.findAllByReceiver(username);
//            Set<SettleUp> settleUpSet2 = settleUpRepository.findAllByGiverAndReceiver(friendName, username);


            settleUpSet = mergeSet(settleUpSet, settleUpSet2);


            ArrayList<HashMap> newList = new ArrayList<>();


            for (SettleUp settleUp : settleUpSet) {
                HashMap<String, Object> map = new HashMap<>();

                map.put("Date", settleUp.getDate());
                map.put("Giver", settleUp.getGiver());
                map.put("Receiver", settleUp.getReceiver());
                map.put("Amount", settleUp.getAmount());

                newList.add(map);

            }




            return ResponseEntity.ok(newList);


        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}

















