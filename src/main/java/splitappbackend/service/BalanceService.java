package splitappbackend.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import splitappbackend.model.Event;
import splitappbackend.model.Group;
import splitappbackend.model.User;
import splitappbackend.repository.EventRepository;
import splitappbackend.repository.GroupRepository;
import splitappbackend.repository.UserRepository;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

@Service("balanceService")
public class BalanceService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    EventRepository eventRepository;





    public ResponseEntity saveBalanceSummaryEqually(String username,
                                                    String eventName,
                                                    BigDecimal totalBalance,
                                                    String paidBy,
                                                    ArrayList<String> members,
                                                    String groupName) {




        BigDecimal roundBalance = new BigDecimal(totalBalance.toBigInteger());



        MathContext mc = new MathContext(3);

        roundBalance = roundBalance.round(mc);




//        Event event = new Event(eventName, new Date(), roundBalance, username);



        //Add members in event & Add event in each members
//        addMemberInEvent(members, event, paidBy);

        // Add event in group
//        addEventDetailInGroup(groupName, event);


        // Manage the debt of each user




        return ResponseEntity.ok(null);
    }
//    private void manageBalance(User user, BigDecimal splitEquallyAmount, String paidBy,  BigDecimal paidByAmount) {
//
//        String username = user.getUserName();
//
//        if (username.equalsIgnoreCase(paidBy)) { // If it is the person that pay
//            BigDecimal amount = user.getYouAreOwed().add(paidByAmount);
//            user.setYouAreOwed(amount);
//
//
//        }
//        else {
//            // set you owe to split Equally Amount
//            BigDecimal amount = user.getYouOwe().add(splitEquallyAmount);
//            user.setYouOwe(amount);
//
//
//        }
//
//
//        // Set totalBalance = youAreOwe - youOwe
//        BigDecimal youOwe = user.getYouOwe();
//        BigDecimal youAreOwed = user.getYouAreOwed();
//        BigDecimal total = youAreOwed.subtract(youOwe);
//        user.setTotalBalance(total);
//        userRepository.save(user);
//
//
//
//
//
//    }

    private void addMemberInEvent(ArrayList<String> members, Event event, String paidBy) {

        MathContext mc = new MathContext(4);

        BigDecimal totalBalance = event.getTotalBalance();


        BigDecimal totalMember = new BigDecimal(members.size());


        BigDecimal splitEquallyAmount = totalBalance.divide(totalMember, mc);




        BigDecimal paidByAmount = splitEquallyAmount.multiply(totalMember.subtract(new BigDecimal("1")));

        BigDecimal amountTrack = new BigDecimal(0.00);
        Integer count = 1;


        Set<User> userSet = new HashSet<>();
        for (String member : members) {
            User user = userRepository.findByUserName(member);
            userSet.add(user);
            Set<Event> userEvents = user.getEvents();
            userEvents.add(event);
            user.setEvents(userEvents);
            userRepository.save(user);

            if (count.equals(totalMember)) { // When it comes to the last round, the paid amount should be equal to the total balance
//                manageBalance(user, totalBalance.subtract(amountTrack), paidBy, paidByAmount);
            }
            else {
//                manageBalance(user, splitEquallyAmount, paidBy, paidByAmount);
            }

            amountTrack = splitEquallyAmount.add(amountTrack);
            count++;


        }
        event.setMembersInEvent(userSet);
        eventRepository.save(event);

    }


//    private void addEventDetailInGroup(String groupName, Event event) {
//        Group group = groupRepository.findByGroupName(groupName);
//        event.setGroup(group);
//        eventRepository.save(event);
//        Set<Event> eventSet = group.getEvents();
//        eventSet.add(event);
//        group.setEvents(eventSet);
//        groupRepository.save(group);
//
//    }
}
