package splitappbackend.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import splitappbackend.model.Event;
import splitappbackend.model.Group;
import splitappbackend.model.User;
import splitappbackend.repository.GroupRepository;
import splitappbackend.repository.UserRepository;

import java.util.*;

@Service("groupService")
public class GroupService {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    UserRepository userRepository;

    public ResponseEntity createGroup(String groupName, List members, String username) {


        if (!groupRepository.existsByGroupName(groupName)) {
            Set<User> members_set = new HashSet<>();
            User me = userRepository.findByUserName(username);
            members_set.add(me);



            for (Object i : members) {
                User user = userRepository.findByUserName(i.toString());
                members_set.add(user);
            }

            Group group = new Group(groupName, members_set);
            groupRepository.save(group);
            return ResponseEntity.ok(null);

        }
        return ResponseEntity.badRequest().body("This groupName has already exists");

    }

    public ResponseEntity getGroup(String username) {
        try {
            User user = userRepository.findByUserName(username);
            Set<Group> groups = user.getGroups();

            ArrayList<String> newList = new ArrayList<>();
            int count = 0;

            for (Group grp : groups) {
                newList.add(grp.getGroupName());
//                Map<String, Object> newMap = new HashMap<>();
//                newMap.put("id", count);
//                newMap.put("name", grp.getGroupName());
//
//                newList.add(newMap);
//                count++;

            }

            return ResponseEntity.ok(newList);
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity getFriendFromGroupName(String username,String groupName) {

        try {

            User me = userRepository.findByUserName(username);
            //check if u r in the group



            Group group = groupRepository.findByGroupName(groupName);




            Set<User> members = group.getMembers();


            ArrayList<String> memberList = new ArrayList<>();


            /// DELETE YOURSELF -- not done


            for (User user: members) {
                String userName = user.getUserName();



                memberList.add(userName);

            }

            if (memberList.contains(me.getUserName())) {
                return ResponseEntity.ok().body(memberList);

            }

            return ResponseEntity.badRequest().body("You should be in the group to proceed");







        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());

        }
    }

    public ResponseEntity getGroupInCommon(String username, String friendName) {

        User user = userRepository.findByUserName(username);

        Set<Group> userGroup = user.getGroups();

        ArrayList<String> commonGroup = new ArrayList<>();


        for (Group group : userGroup) {
            Set<User> userSet = group.getMembers();
            int count = 0;

            for (User each : userSet) {
                if (each.getUserName().equalsIgnoreCase(username)) {
                    count++;
                }
                else if (each.getUserName().equalsIgnoreCase(friendName)) {
                    count++;
                }

            }
            if (count == 2) {
                commonGroup.add(group.getGroupName());
            }
        }


        return ResponseEntity.ok(commonGroup);
    }

    public ResponseEntity getEventsInGroup(String groupName) {

        try {


            Group group = groupRepository.findByGroupName(groupName);

            Set<Event> events = group.getEvents();

            ArrayList<HashMap> summary = new ArrayList<>();


            for (Event event : events) {
                HashMap<String, Object> eventInfo = new HashMap<>();

                eventInfo.put("eventUUID", event.getEventUUID());
                eventInfo.put("createdBy", event.getCreatedBy());
                eventInfo.put("date", event.getDate());
                eventInfo.put("eventName", event.getEventName());
                eventInfo.put("totalBalance", event.getTotalBalance());
                eventInfo.put("members", getMemberName(event.getMembersInEvent()));

                summary.add(eventInfo);

            }

            return ResponseEntity.ok(summary);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    private ArrayList<String> getMemberName(Set<User> users) {

        ArrayList<String> name = new ArrayList<>();

        for (User user : users) {
            name.add(user.getUserName());

        }

        return name;
    }

    public ResponseEntity addFriendInGroup(String username, String friendName, String groupName) {

        try {
            User user = userRepository.findByUserName(username);
            User friend = userRepository.findByUserName(friendName);

            Group group = groupRepository.findByGroupName(groupName);

            Set<User> members = group.getMembers();
            members.add(friend);



            group.setMembers(members);

            groupRepository.save(group);
//            group.setMembers(members);

//
//            user.getFriends().add(friend);
//            userRepository.save(user);
//            friend.getFriends().add(user);
//            userRepository.save(friend);

            return ResponseEntity.ok(null);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());

        }


    }

    public ResponseEntity getFriendLeft(String username, ArrayList<String> member) {

        ArrayList<String> newFriend = new ArrayList<>();



        try {
            Set<User> allFriend = userRepository.findByUserName(username).getFriends();


            for (User friend : allFriend) {
                String name = friend.getUserName();

                if (!name.equalsIgnoreCase(username)) {

                    if (!member.contains(name)) {
                        newFriend.add(name);

                    }
                }


            }


            return ResponseEntity.ok(newFriend);







        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }




//    public ResponseEntity getOweSummaryInGroup(String username, String groupName) {
//
//        Group group = groupRepository.findByGroupName(groupName);
//
//        Set<User> users = group.getMembers();
//
//
//
//
//
//
//    }



}
















