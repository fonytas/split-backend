package splitappbackend.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import splitappbackend.model.User;
import splitappbackend.repository.UserRepository;

import java.util.*;

@Service("friendService")
public class FriendService {

    @Autowired
    UserRepository userRepository;




    public boolean checkAuthKey(String authKey) {
        return userRepository.existsByAuthKey(authKey);
    }

    public ResponseEntity addFriend(String username, String friendName) {

        try {
            User user = userRepository.findByUserName(username);
            User friend = userRepository.findByUserName(friendName);


            // Check if user not adding yourself

            if (!user.getUserName().equals(friend.getUserName())) {

                Set<User> user_friend = user.getFriends();
                Set<User> friend_user = friend.getFriends();

                // check if user not adding the existing friend
                for (User u : user_friend) {
                    if (u.getUserName().equals(friendName)) {
                        return ResponseEntity.badRequest().body("Oops! " + friendName +" is already be your friend");
                    }
                }

                user_friend.add(friend);
                friend_user.add(user);

                user.setFriends(user_friend);
                friend.setFriends(friend_user);

                userRepository.save(user);
                userRepository.save(friend);

                return ResponseEntity.ok(null);
            }
            return ResponseEntity.badRequest().body("You cannot add yourself.");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(friendName + " does not exist");
        }
    }

    public ResponseEntity getFriends(String username) {
        try {
            User user = userRepository.findByUserName(username);
            Set<User> friends = user.getFriends();
            ArrayList<String> newList = new ArrayList<>();

            int count = 0;

            for (User frd : friends) {
//                Map<String, Object> newMap = new HashMap<>();
//                newMap.put("id", count);
//                newMap.put("name", frd.getUserName());
//
//                newList.add(newMap);
                newList.add(frd.getUserName());

                count++;

            }
            return ResponseEntity.ok(newList);

        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity getFriendsName(String username) {

        try {
            User user = userRepository.findByUserName(username);
            Set<User> friends = user.getFriends();
            ArrayList<String> newList = new ArrayList<>();


            for (User frd : friends) {

                newList.add(frd.getUserName());

            }
            return ResponseEntity.ok(newList);



        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());

        }
    }

}
