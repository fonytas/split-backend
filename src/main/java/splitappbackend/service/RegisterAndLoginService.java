package splitappbackend.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import splitappbackend.model.User;
import splitappbackend.repository.UserRepository;

import java.util.HashMap;
import java.util.UUID;

@Service("registerAndLoginService")
public class RegisterAndLoginService {

    @Autowired
    private UserRepository userRepository;


    public boolean userNameExist(String userName) {
        return userRepository.existsByUserName(userName);

    }

    public ResponseEntity userRegister(String userName, String password) {
        try {
            String authKey = UUID.randomUUID().toString();
            User user = new User(userName, password, authKey);
            userRepository.save(user);
            return ResponseEntity.ok(null);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Cannot register");

        }
    }

    public ResponseEntity userLogin(String userName, String password) {
        try {
            User user = userRepository.findByUserName(userName);
            boolean passwordMatch = checkPasswordMatch(user.getPassword(),password);

            if (passwordMatch) {
                HashMap<String, String> info = new HashMap<>();
                info.put("API_KEY", user.getAuthKey());
                return ResponseEntity.ok().body(info);

            }

            return ResponseEntity.badRequest().body("Incorrect Password");

        } catch (Exception e) {
             return ResponseEntity.badRequest().body(e.getMessage());

        }
    }

    private boolean checkPasswordMatch(String actualPassword, String enteredPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(enteredPassword, actualPassword);

    }

//    public boolean emailExist(String email) {
//        return userRepository.existsByEmail(email);
//    }

//    public String canRegister(String userName) {
//
//
//
////        if (userNameExist(userName)) return "Username already exist";
//
//
//    }
}
