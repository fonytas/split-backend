package splitappbackend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import splitappbackend.model.SettleUp;

import java.util.Set;


@Repository
public interface SettleUpRepository extends JpaRepository<SettleUp, Integer> {


//    Set<Event> findAllBy

//    Set<Event> findByGroupName(String groupName);
    Set<SettleUp> findAllByGiverAndReceiver(String giver, String receiver);

    Set<SettleUp> findAllByGiver(String giver);
    Set<SettleUp> findAllByReceiver(String receiver);





}
