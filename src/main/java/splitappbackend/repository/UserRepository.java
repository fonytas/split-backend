package splitappbackend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import splitappbackend.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    boolean existsByUserName(String userName);

    User findByUserName(String userName);

    boolean existsByAuthKey(String authKey);

    User findByAuthKey(String authKey);


//    boolean existsByEmail(String email);


}
