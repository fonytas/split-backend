package splitappbackend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import splitappbackend.model.Owe;

import java.util.Set;


@Repository
public interface OweRepository extends JpaRepository<Owe, Integer> {

    Set<Owe> findAllByPaidByUUID(Long paidByUUID);

    Set<Owe> findAllByDebtorUUID(Long debtorUUID);

    Set<Owe> findAllByPaidByUUIDAndDebtorUUID(Long paidByUUID, Long debtorUUID);






}
