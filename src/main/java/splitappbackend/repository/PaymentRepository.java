package splitappbackend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import splitappbackend.model.Owe;
import splitappbackend.model.Payment;


@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {






}
