package splitappbackend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import splitappbackend.model.Event;
import splitappbackend.model.Group;

import java.util.Set;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {

    boolean existsByGroupName(String groupName);

    Group findByGroupName(String groupName);



//    Set<Event> findByGroupName(String groupName);
//    Set<Group> find
    



}
