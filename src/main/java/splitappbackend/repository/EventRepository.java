package splitappbackend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import splitappbackend.model.Event;
import splitappbackend.model.Group;

import java.util.Set;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {


//    Set<Event> findAllBy

//    Set<Event> findByGroupName(String groupName);



}
