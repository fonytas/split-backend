package splitappbackend.configuration;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;
import splitappbackend.model.User;
import splitappbackend.repository.UserRepository;
import splitappbackend.service.RegisterAndLoginService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import java.util.stream.Collectors;

public class PreTokenAuthenticationFilter extends GenericFilterBean {
    private final Logger logger = LoggerFactory.getLogger(PreTokenAuthenticationFilter.class.getName());

    private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();

    private TokenAuthenticationProvider tokenAuthenticationProvider;


    public PreTokenAuthenticationFilter(TokenAuthenticationProvider authenticationManager) {
        this.tokenAuthenticationProvider = authenticationManager;
    }

    @Autowired
    RegisterAndLoginService registerAndLoginService;

    @Autowired
    UserRepository userRepository;



    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {



        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        logger.info(String.valueOf(request.getRequestURL()));

        String requestURI = request.getRequestURI();





        if (!requestURI.equals("/user-login") || !requestURI.equals("/user-register")) {
            try {
                String apiKey = null;
                apiKey = request.getHeader("API_KEY");


                AbstractAuthenticationToken authRequest = new PreAuthenticatedAuthenticationToken(null, apiKey);

                authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
                authRequest.setAuthenticated(true);
                Authentication authResult = tokenAuthenticationProvider.authenticate(authRequest);
                SecurityContextHolder.getContext().setAuthentication(authResult);
            } catch (AuthenticationException | NullPointerException authException) {

            }

        }





        chain.doFilter(request, response);











    }
}
