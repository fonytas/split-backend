package splitappbackend.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseLogin {

    private String message;
    private boolean isLogin;

    public ResponseLogin() {
        this.message = "You are not logged in";
        this.isLogin = false;
    }

    public ResponseLogin(String message, boolean isLogin) {
        this.message = message;
        this.isLogin = isLogin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        }catch (JsonProcessingException e){
            return null;
        }
    }
}
