package splitappbackend.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import splitappbackend.model.User;
import splitappbackend.repository.UserRepository;
import splitappbackend.service.RegisterAndLoginService;

import javax.servlet.ServletRequest;

@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    UserRepository userRepository;




    private final Logger logger = LoggerFactory.getLogger(TokenAuthenticationProvider.class.getName());
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        /// check against db




        if (authentication.getCredentials().toString() == null) {

            throw new AuthenticationCredentialsNotFoundException("API Key not found.");
        }


        String authKey = authentication.getCredentials().toString();

        if (!userRepository.existsByAuthKey(authKey)) {
            throw new UsernameNotFoundException("User does not exists");
        }


        User user = userRepository.findByAuthKey(authKey);
        String username = user.getUserName();

//        return new User

        return new UsernamePasswordAuthenticationToken(username,authKey);

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
