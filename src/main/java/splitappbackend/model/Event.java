package splitappbackend.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "events")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eventsUUID", updatable = false, nullable = false)
    private Long eventUUID;

    private String eventName;

    private Date date;

    private BigDecimal totalBalance;

    private String createdBy;

    private String paymentId;

//    private ArrayList<String> memberName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "groupUUID")
    private Group group;




    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="EVENT_MEMBER",
            joinColumns = @JoinColumn(name = "eventUUID"),
            inverseJoinColumns = {@JoinColumn(name = "userUUID")})
    private Set<User> membersInEvent;


    public Event() {}

    public Event(String eventName, Date date, BigDecimal totalBalance, String createdBy, String paymentId) {
        this.eventName = eventName;
        this.date = date;
        this.totalBalance = totalBalance;
        this.createdBy = createdBy;
        this.paymentId = paymentId;
//        this.memberName = memberName;
//        this.group = group;
    }



    public Long getEventUUID() {
        return eventUUID;
    }

    public void setEventUUID(Long eventUUID) {
        this.eventUUID = eventUUID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Set<User> getMembersInEvent() {
        return membersInEvent;
    }

    public void setMembersInEvent(Set<User> membersInEvent) {
        this.membersInEvent = membersInEvent;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    //    public ArrayList<String> getMemberName() {
//        return memberName;
//    }
//
//    public void setMemberName(ArrayList<String> memberName) {
//        this.memberName = memberName;
//    }

//    public Set<User> getUsers() {
//        return users;
//    }
//
//    public void setUsers(Set<User> users) {
//        this.users = users;
//    }
}
