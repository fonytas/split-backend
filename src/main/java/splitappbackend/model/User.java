package splitappbackend.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userUUID", updatable = false, nullable = false)
    private Long userUUID;

    @Column(unique = true)
    private String userName;

    private String password;

    private String authKey;




    @ManyToMany(cascade={CascadeType.ALL})
    @JoinTable(name="USER_FRIEND",
            joinColumns= @JoinColumn(name="userName"),
            inverseJoinColumns={@JoinColumn(name="friendName") })
    private Set<User> friends = new HashSet<User>();




    @ManyToMany(mappedBy = "members")
    private Set<Group> groups;

    @ManyToMany(mappedBy = "membersInEvent")
    private Set<Event> events;

// delete join table

    public User() {}


    public User(String userName, String password, String authKey) {
        this.userName = userName;
        this.password = password;
        this.authKey = authKey;

    }


    public Long getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(Long userUUID) {
        userUUID = userUUID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public Set<User> getFriends() {
        return friends;
    }

    public void setFriends(Set<User> friends) {
        this.friends = friends;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }



    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    //    public Set<Event> getEvents() {
//        return events;
//    }
//
//    public void setEvents(Set<Event> events) {
//        this.events = events;
//    }
}
