package splitappbackend.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


@Entity
public class SettleUp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "settleUpUUID", updatable = false, nullable = false)
    private Long settleUpUUID;

    private Date date;

    private BigDecimal amount;

    private String giver;

    private String receiver;

    public SettleUp() {}

    public SettleUp(BigDecimal amount, String giver, String receiver) {
        this.amount = amount;
        this.giver = giver;
        this.receiver = receiver;
        this.date = new Date();
    }


    public Long getSettleUpUUID() {
        return settleUpUUID;
    }

    public void setSettleUpUUID(Long settleUpUUID) {
        this.settleUpUUID = settleUpUUID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getGiver() {
        return giver;
    }

    public void setGiver(String giver) {
        this.giver = giver;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }
}
