package splitappbackend.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "groups")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "groupUUID", updatable = false, nullable = false)
    private Long groupUUID;

    private String groupName;


    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="GROUP_MEMBER",
            joinColumns = @JoinColumn(name = "groupUUID"),
            inverseJoinColumns = {@JoinColumn(name = "userUUID")})
    private Set<User> members;



    @OneToMany(cascade = CascadeType.ALL,
                mappedBy = "group",
                fetch = FetchType.LAZY)
    private Set<Event> events;


    public Group(String groupName, Set<User> members) {
        this.groupName = groupName;
        this.members = members;
    }

    public Group() {}


    public Long getGroupUUID() {
        return groupUUID;
    }

    public void setGroupUUID(Long groupUUID) {
        this.groupUUID = groupUUID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }
}
