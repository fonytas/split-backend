package splitappbackend.model;


import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Owe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oweUUID", updatable = false, nullable = false)
    private Long oweUUID;

    private Long debtorUUID;

    private Long paidByUUID;

    private BigDecimal amount;

    private Long groupUUID;



    public Owe() {}

    public Owe(Long debtorUUID, Long paidByUUID, BigDecimal amount, Long groupUUID) {
        this.debtorUUID = debtorUUID;
        this.paidByUUID = paidByUUID;
        this.amount = amount;
        this.groupUUID = groupUUID;
    }

    public Owe(Long debtorUUID, Long paidByUUID, BigDecimal amount) {
        this.debtorUUID = debtorUUID;
        this.paidByUUID = paidByUUID;
        this.amount = amount;
    }


    public Long getOweUUID() {
        return oweUUID;
    }

    public void setOweUUID(Long oweUUID) {
        this.oweUUID = oweUUID;
    }

    public Long getDebtorUUID() {
        return debtorUUID;
    }

    public void setDebtorUUID(Long debtorUUID) {
        this.debtorUUID = debtorUUID;
    }

    public Long getPaidByUUID() {
        return paidByUUID;
    }

    public void setPaidByUUID(Long paidByUUID) {
        this.paidByUUID = paidByUUID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getGroupUUID() {
        return groupUUID;
    }

    public void setGroupUUID(Long groupUUID) {
        this.groupUUID = groupUUID;
    }
}
