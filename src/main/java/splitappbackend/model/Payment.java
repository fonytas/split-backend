package splitappbackend.model;


import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paymentUUID", updatable = false, nullable = false)
    private Long paymentUUID;

    private String billId;

    private String username;

    private BigDecimal paidByAmount;

    private BigDecimal amount;

    private Long groupUUID;


    public Payment() {}

    public Payment(String billId, String username, BigDecimal paidByAmount, BigDecimal amount, Long groupUUID) {
        this.billId = billId;
        this.username = username;
        this.paidByAmount = paidByAmount;
        this.amount = amount;
        this.groupUUID = groupUUID;
    }

    public Long getPaymentUUID() {
        return paymentUUID;
    }

    public void setPaymentUUID(Long paymentUUID) {
        this.paymentUUID = paymentUUID;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public BigDecimal getPaidByAmount() {
        return paidByAmount;
    }

    public void setPaidByAmount(BigDecimal paidByAmount) {
        this.paidByAmount = paidByAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getGroupUUID() {
        return groupUUID;
    }

    public void setGroupUUID(Long groupUUID) {
        this.groupUUID = groupUUID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
