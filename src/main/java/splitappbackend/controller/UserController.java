package splitappbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import splitappbackend.service.PaymentService;
import splitappbackend.service.RegisterAndLoginService;

import java.security.Principal;
import java.util.HashMap;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class UserController {

    @Autowired
    private RegisterAndLoginService registerAndLoginService;

    @Autowired
    private PaymentService paymentService;


    @GetMapping("/whoami")
    public ResponseEntity whoami(Principal principal) {
        String username = principal != null? principal.getName() : "Nobody";
        return ResponseEntity.ok(username);
    }

    @GetMapping("/get-your-balance-summary")
    public ResponseEntity getYourBalanceSummary(Principal principal) {
        String username = principal.getName();


        return paymentService.getYourBalanceSummary(username);

    }

    @GetMapping("/get-balance-summary-of-friend")
    public ResponseEntity getBalanceSummaryOfFriends(Principal principal, String friendName) {
        String username = principal.getName();

        return paymentService.getBalanceSummaryOfFriends(username, friendName);



    }


    @RequestMapping(method = RequestMethod.POST, value = "/user-register")
    public ResponseEntity studentRegister(@RequestBody HashMap body) {


        String userName = body.get("username").toString();
        String password = body.get("password").toString();

        if (!registerAndLoginService.userNameExist(userName)) { // if  username not exist --> allow to register
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            return registerAndLoginService.userRegister(userName, passwordEncoder.encode(password));

        } else {
            return ResponseEntity.badRequest().body("Username: " + userName + " has already existed");
        }

    }



    @RequestMapping(method = RequestMethod.POST, value = "/user-login")
    public ResponseEntity studentLogin(@RequestBody HashMap body) {


        String userName = body.get("username").toString();
        String password = body.get("password").toString();

        if (registerAndLoginService.userNameExist(userName)) {
            return registerAndLoginService.userLogin(userName, password);
        }
        return ResponseEntity.badRequest().body("Username: " + userName + " does not exist");

    }





}
