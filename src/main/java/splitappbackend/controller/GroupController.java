package splitappbackend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import splitappbackend.service.FriendService;
import splitappbackend.service.GroupService;
import splitappbackend.service.RegisterAndLoginService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class GroupController {

    @Autowired
    FriendService friendService;

    @Autowired
    GroupService groupService;


    @RequestMapping(method = RequestMethod.POST, value = "/create-group")
    public ResponseEntity createGroup(@RequestBody HashMap body, String groupName, Principal principal) {


//        if (friendService.checkAuthKey(authKey)) {
        String username = principal.getName();
        List members = (List) body.get("members");
        return groupService.createGroup(groupName, members, username);

//        }
//        return ResponseEntity.badRequest().body("xxx Access Denied xxx");

    }

    @PostMapping("/add-friend-in-group")
    public ResponseEntity addFriendInGroup(Principal principal, String friendName, String groupName) {

        String username = principal.getName();

        return groupService.addFriendInGroup(username, friendName, groupName);



    }



    @GetMapping("/get-group")
    public ResponseEntity getGroup(Principal principal) {
//        if (friendService.checkAuthKey(authKey)) {
        String username = principal.getName();
//        System.out.println("======" + username);
        return groupService.getGroup(username);

//        }
//        return ResponseEntity.badRequest().body("xxx Access Denied xxx");

    }

    @GetMapping("/get-friend-from-groupName")
    public ResponseEntity getFriendFromGroupName(Principal principal, String groupName) {

        String username = principal.getName();

        return groupService.getFriendFromGroupName(username, groupName);


//        }
//        return ResponseEntity.badRequest().body("xxx Access Denied xxx");
    }

    @GetMapping("/get-group-in-common")
    public ResponseEntity getGroupInCommon(Principal principal, String friendName) {

        String username = principal.getName();

        return groupService.getGroupInCommon(username, friendName);

    }

    @GetMapping("/get-events-in-group")
    public ResponseEntity getEventsInGroup(String groupName) {

        return groupService.getEventsInGroup( groupName);

    }


//    @RequestMapping(method = RequestMethod.POST, value = "/create-group")
//    public ResponseEntity createGroup(@RequestBody HashMap body, String groupName, Principal principal) {

//    @GetMapping("/get-friend-left")
//    public ResponseEntity getFriendLeft(Principal principal, ArrayList<String> member) {
//
//        String username = principal.getName();
//
//        return groupService.getFriendLeft(username, member);
//
//
//
//    }

    @RequestMapping(method = RequestMethod.POST, value = "/get-friend-left")
    public ResponseEntity getFriendLeft(Principal principal, @RequestBody HashMap body) {

        ArrayList<String> member = (ArrayList<String>) body.get("member");
        String username = principal.getName();

        return groupService.getFriendLeft(username, member);


    }





//    @GetMapping("/get-owe-summary-in-group")
//    public ResponseEntity getOweSummaryInGroup(Principal principal, String gropName) {
//        String username = principal.getName();
//
//        return groupService.getOweSummaryInGroup(username, gropName);
//
//
//
//    }





}
