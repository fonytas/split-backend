package splitappbackend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import splitappbackend.service.BalanceService;
import splitappbackend.service.FriendService;
import splitappbackend.service.PaymentService;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @Autowired
    FriendService friendService;





    @RequestMapping(method = RequestMethod.POST, value = "/save-bill")
    public ResponseEntity saveBill(@RequestBody HashMap body, Principal principal) {

        String username = principal.getName();




        String eventName = body.get("eventName").toString();
        BigDecimal totalBalance = new BigDecimal(body.get("totalBalance").toString());

        String paidBy = body.get("paidBy").toString();
        ArrayList<String> members = (ArrayList<String>) body.get("members");
        String groupName = body.get("groupName").toString();
//        ArrayList<HashMap> summary = (ArrayList<HashMap>) body.get("summary");
        HashMap<String, Object> summary = (HashMap) body.get("summary");


        return paymentService.saveBalanceEqually(username, eventName, totalBalance, paidBy, members, groupName, summary);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/settle-up")
    public ResponseEntity settleUp(@RequestBody HashMap body, Principal principal) {
//    public Owe(Long debtorUUID, Long paidByUUID, BigDecimal amount, Long groupUUID) {

        String username = principal.getName();

        String friendName = body.get("friendName").toString();

        BigDecimal totalBalance = new BigDecimal(body.get("totalBalance").toString());

//        System.out.println(totalBalance);



        return paymentService.settleUp(username, friendName, totalBalance);


    }

    @GetMapping("/get-settle-up-history")
    public ResponseEntity getSettleUpHistory(Principal principal) {
        String username = principal.getName();

        return paymentService.getSettleUpHistory(username);


    }




}


