package splitappbackend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import splitappbackend.service.FriendService;

import java.security.Principal;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class FriendController {

    @Autowired
    FriendService friendService;

    @GetMapping("/test")
    public ResponseEntity test(Principal principal) {
        System.out.println("this is test");
        String username = principal != null? principal.getName() : "Nobody";
        return ResponseEntity.ok(username);
    }

    @PostMapping("/add-friend")
    public ResponseEntity addFriend(Principal principal, String friendName) {
        String username = principal.getName();

        return friendService.addFriend(username, friendName);



    }

    @GetMapping("get-friends")
    public ResponseEntity getFriends(Principal principal) {

        String username = principal.getName();
        return friendService.getFriends(username);

    }

    @GetMapping("get-friends-name")
    public ResponseEntity getFriendsName(Principal principal) {

        String username = principal.getName();
        return friendService.getFriendsName(username);

    }

}
