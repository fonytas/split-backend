


FROM openjdk:8
WORKDIR /app
ADD target/split-app-backend-1.0-SNAPSHOT.jar splitappbackend.jar
ADD src/main/resources/application.properties application.properties
EXPOSE 8080
#ARG JAR_FILE
#COPY ${JAR_FILE} simpleobjectstorage.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "splitappbackend.jar"]